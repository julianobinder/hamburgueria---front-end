import { User } from "./user.model";

export class Ticket {
  public id: string;
  public number: number;
  public title: string;
  public description: string;
  public status: string;
  public priority: string;
  public image: string;
  public user: User;
  public assignedUser: User;
  public date: string;
  public changes: Array<string>;
  public tipo: string;
  public customer: User;
  public enviarEmail: boolean;

  constructor() {}
}
