import { User } from "./user.model";

//package com.binder.helpdesk.api.security.model; CurrentUser// api-backend
export class CurrentUser {
    public token : string;
    public user : User;
}
