import { User } from 'src/app/model/user.model';

export class SendFilesCustomer {
    public id: string;
    public user: User;
    public date: string;
    public mesAno: string;
    public subject: string;
    public tipo: string;
    public number: number;
    public description: string;
    public file: string;

    constructor() {}
  }





