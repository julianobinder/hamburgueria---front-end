import { UserListComponent } from './components/user-list/user-list.component';
import { AuthGuard } from './components/security/auth.guard';
import { LoginComponent } from './components/security/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders, Component } from '@angular/core';
import { UserNewComponent } from './components/user-new/user-new.component';
import { TicketNewComponent } from './components/ticket-new/ticket-new.component';
import { TicketListComponent } from './components/ticket-list/ticket-list.component';
import { TicketDetailComponent } from './components/ticket-detail/ticket-detail.component';
import { SummaryComponent } from './components/summary/summary.component';
import { ForgotPasswordComponent } from './components/security/forgot-password/forgot-password.component';
import { ArquivosComponent } from './components/arquivos/arquivos.component';
import { SendFilesCustomerComponent } from './components/send-files-customer/send-files-customer.component';
import { FilesReceivedComponent } from './components/files-received/files-received.component';
import { ListSendFilesComponent } from './components/list-send-files/list-send-files.component';

//com o AuthGuard só tem acesso ao sistema depois de logado.
export const ROUTES: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'forgot-password/:id', component: ForgotPasswordComponent },
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'user-new', component: UserNewComponent, canActivate: [AuthGuard] },
  { path: 'user-new/:id', component: UserNewComponent, canActivate: [AuthGuard] },
  { path: 'user-list', component: UserListComponent, canActivate: [AuthGuard] },
  { path: 'ticket-new', component: TicketNewComponent, canActivate: [AuthGuard] },
  { path: 'ticket-new/:id', component: TicketNewComponent, canActivate: [AuthGuard] },
  { path: 'ticket-list', component: TicketListComponent, canActivate: [AuthGuard] },
  { path: 'ticket-detail/:id', component: TicketDetailComponent, canActivate: [AuthGuard] },
  { path: 'summary', component: SummaryComponent, canActivate: [AuthGuard] },
  { path: 'arquivos/:tipo', component: ArquivosComponent, canActivate: [AuthGuard] },
  { path: 'send-files-customer', component: SendFilesCustomerComponent, canActivate: [AuthGuard] },
  { path: 'list-send-files', component: ListSendFilesComponent, canActivate: [AuthGuard] },
  { path: 'files-received', component: FilesReceivedComponent, canActivate: [AuthGuard] }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(ROUTES);
