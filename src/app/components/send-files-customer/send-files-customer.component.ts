import { SendFilesCustomer } from './../../model/send-files';
import { SendFileService } from './../../services/sendfiles.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SharedService } from 'src/app/services/shared.service';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { ResponseApi } from 'src/app/model/response-api';

@Component({
  selector: 'app-send-files-customer',
  templateUrl: './send-files-customer.component.html',
  styleUrls: ['./send-files-customer.component.css']
})
export class SendFilesCustomerComponent implements OnInit {

  //após efetuar a inclusão resetar o form
  @ViewChild("form")
  form: NgForm;

  isLoading = false;

  sendFilesCustomer = new SendFilesCustomer();
  shared: SharedService;
  message: {};
  classCss: {};


  constructor(
    private sendFileService: SendFileService
  ) {

  }

  ngOnInit() {

  }

  save() {
    this.sendFileService.update(this.sendFilesCustomer)
      .subscribe((r: any) => {
        this.sendFilesCustomer = new SendFilesCustomer();
        this.form.resetForm();
        this.showMessage({
          type: "success",
          text: `Registered successfully`
        });
        this.isLoading = false;
      }, err => {
        this.showMessage({
          type: "error",
          text: err["error"]["errors"][0]
        });
        this.isLoading = false;
      });
  }

  //metodo pega a imagem transforma em byte para salvar no mongo
  onFileChange(event): void {
    if (event.target.files[0].size > 2000000) {
      this.showMessage({
        type: "error",
        text: "Maximum image size is 2MB"
      });
    } else {
      this.sendFilesCustomer.file = "";
      var reader = new FileReader();
      reader.onloadend = (e: Event) => {
        this.sendFilesCustomer.file = <string>reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  private showMessage(message: { type: string; text: string }): void {
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    }, 3000);
  }

  private buildClasses(type: string): void {
    this.classCss = {
      alert: true
    };
    this.classCss["alert-" + type] = true;
  }

  getFromGroupClass(isInvalid: boolean, isDirty): {} {
    return {
      "form-group": true,
      "has-error": isInvalid && isDirty,
      "has-success": isInvalid && isDirty
    };
  }

}
