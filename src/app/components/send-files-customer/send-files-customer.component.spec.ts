import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendFilesCustomerComponent } from './send-files-customer.component';

describe('SendFilesCustomerComponent', () => {
  let component: SendFilesCustomerComponent;
  let fixture: ComponentFixture<SendFilesCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendFilesCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendFilesCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
