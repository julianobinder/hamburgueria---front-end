import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSendFilesComponent } from './list-send-files.component';

describe('ListSendFilesComponent', () => {
  let component: ListSendFilesComponent;
  let fixture: ComponentFixture<ListSendFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSendFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSendFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
