import { Component, OnInit } from "@angular/core";
import { SendFileService } from "src/app/services/sendfiles.service";
import { ConfirmationService } from "primeng/api";

@Component({
  selector: "app-list-send-files",
  templateUrl: "./list-send-files.component.html",
  styleUrls: ["./list-send-files.component.css"]
})
export class ListSendFilesComponent implements OnInit {
  pedidos: any[] = [];
  editar = false;

  pedido: any = {};

  constructor(
    private sendFileSvc: SendFileService,
    private confirmationSvc: ConfirmationService
  ) {}

  ngOnInit() {
    this.carregar();
  }

  carregar() {
    this.sendFileSvc.findAll().subscribe((r: any) => (this.pedidos = r.data));
  }

  edit(ped) {
    this.pedido = ped;
    this.editar = true;
  }

  save() {
    this.sendFileSvc.update(this.pedido).subscribe(r => {
      this.carregar();
      alert("Pedido alterado com sucesso!");
      this.editar = false;
      this.pedido = {};
    });
  }

  delete(id) {
    this.confirmationSvc.confirm({
      message: "Confirma deletar este pedido ?",
      accept: () => {
        this.sendFileSvc.delete(id).subscribe(r => {
          alert("Arquivo deletado com sucesso!!");
          this.carregar();
        });
      }
    });
  }
}
