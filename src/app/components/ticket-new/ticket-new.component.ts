import { ActivatedRoute } from "@angular/router";
import { SharedService } from "./../../services/shared.service";
import { Ticket } from "./../../model/ticket.model";
import { Component, OnInit } from "@angular/core";
import { ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { TicketService } from "src/app/services/ticket.service";
import { ResponseApi } from "src/app/model/response-api";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-ticket-new",
  templateUrl: "./ticket-new.component.html",
  styleUrls: ["./ticket-new.component.css"]
})
export class TicketNewComponent implements OnInit {
  //após efetuar a inclusão resetar o form
  @ViewChild("form")
  form: NgForm;


  isLoading = false;

  ticket = new Ticket();
  shared: SharedService;
  message: {};
  classCss: {};

  usuarios: any[];

  constructor(
    private ticketService: TicketService,
    private userSvc: UserService,
    private route: ActivatedRoute
  ) {
    this.shared = SharedService.getInstance();
  }

  ngOnInit() {
    const id = this.route.snapshot.params["id"];
    if (id !== undefined) {
      this.findById(id);
    }

    this.userSvc.findAll2().subscribe((r: any) => {
      this.usuarios = r.data;
      this.ticket.customer = this.usuarios[0];
    });
  }

  findById(id: string) {
    this.ticketService.findById(id).subscribe(
      (responseApi: ResponseApi) => {
        this.ticket = responseApi.data;
      },
      err => {
        this.showMessage({
          type: "error",
          text: err["error"]["errors"][0]
        });
      }
    );
  }

  register() {
    this.message = {};
    this.isLoading = true;
    this.ticketService.createOrUpdate(this.ticket).subscribe(
      (responseApi: ResponseApi) => {
        this.ticket = new Ticket();
        let ticket: Ticket = responseApi.data;
        this.ticket = new Ticket();
        this.form.resetForm();
        this.showMessage({
          type: "success",
          text: `Registered ${ticket.title} successfully`
        });
        this.isLoading = false;
      },
      err => {
        console.log("Erro --> ", err);
        this.showMessage({
          type: "error",
          text: err["error"]["errors"][0]
        });
        this.isLoading = false;
      }
    );
  }

  //metodo pega a imagem transforma em byte para salvar no mongo
  onFileChange(event): void {
    if (event.target.files[0].size > 2000000) {
      this.showMessage({
        type: "error",
        text: "Maximum image size is 2MB"
      });
    } else {
      this.ticket.image = "";
      var reader = new FileReader();
      reader.onloadend = (e: Event) => {
        this.ticket.image = <string>reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  private showMessage(message: { type: string; text: string }): void {
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    }, 3000);
  }

  private buildClasses(type: string): void {
    this.classCss = {
      alert: true
    };
    this.classCss["alert-" + type] = true;
  }

  getFromGroupClass(isInvalid: boolean, isDirty): {} {
    return {
      "form-group": true,
      "has-error": isInvalid && isDirty,
      "has-success": isInvalid && isDirty
    };
  }
}
