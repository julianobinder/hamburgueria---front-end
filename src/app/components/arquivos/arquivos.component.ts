import { TicketService } from './../../services/ticket.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
@Component({
  selector: 'app-arquivos',
  templateUrl: './arquivos.component.html',
  styleUrls: ['./arquivos.component.css']
})
export class ArquivosComponent implements OnInit {
  tipo: string;
  message: '';

  mesAno = moment(new Date()).format('MM-YYYY');

  listTicket: any[] = [];

  constructor(
    private router: ActivatedRoute,
    private ticketSvc: TicketService
  ) { }

  ngOnInit() {
    this.tipo = this.router.snapshot.params['tipo'];

    this.router.params.subscribe(p => {
      this.tipo = p.tipo;
      this.buscar();
    });
  }

  buscar() {
    this.ticketSvc
      .findByDateAndTipo(this.mesAno, this.tipo)
      .subscribe((r: any) => {
        this.listTicket = r.data;
      });
  }
}
