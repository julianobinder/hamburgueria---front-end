import { ResponseApi } from "./../../model/response-api";
import { ActivatedRoute } from "@angular/router";
import { SharedService } from "./../../services/shared.service";
import { NgForm } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Ticket } from "src/app/model/ticket.model";
import { TicketService } from "src/app/services/ticket.service";
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: "app-ticket-detail",
  templateUrl: "./ticket-detail.component.html",
  styleUrls: ["./ticket-detail.component.css"]
})
export class TicketDetailComponent implements OnInit {
  ticket = new Ticket();
  shared: SharedService;
  message: {};
  classCss: {};

  urlPdf: string;

  constructor(
    public sanitizer: DomSanitizer,
    private ticketService: TicketService,
    private route: ActivatedRoute
  ) {
    this.shared = SharedService.getInstance();
  }

  ngOnInit() {
    let id: string = this.route.snapshot.params["id"];
    if (id != undefined) {
      this.findById(id);
    }
  }

  findById(id: string) {
    this.ticketService.findById(id).subscribe(
      (responseApi: ResponseApi) => {
        this.ticket = responseApi.data;
        this.ticket.date = new Date(this.ticket.date).toISOString();

        const type = this.getType();

        if (type.includes("pdf") || type.includes("text")) {
          this.getFile();
        }
      },
      err => {
        this.showMessage({
          type: "error",
          text: "Erro" /*err['error']['errors'][0]*/
        });
      }
    );
  }

  private showMessage(message: { type: string; text: string }): void {
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    }, 3000);
  }

  private buildClasses(type: string): void {
    this.classCss = {
      alert: true
    };
    this.classCss["alert-" + type] = true;
  }

  changeStatus(status: string): void {
    this.ticketService.changeStatus(status, this.ticket).subscribe(
      (responseApi: ResponseApi) => {
        this.ticket = responseApi.data;
        this.ticket.date = new Date(this.ticket.date).toISOString();
        this.showMessage({
          type: "success",
          text: "Successfully changed status"
        });
      },
      err => {
        this.showMessage({
          type: "error",
          text: err["error"]["errors"][0]
        });
      }
    );
  }

  getFormGroupClass(isInvalid: boolean, isDirty: boolean): {} {
    return {
      "form-group": true,
      "has-error": isInvalid && isDirty,
      "has-success": !isInvalid && isDirty
    };
  }

  onFileChange(event): void {
    if (event.target.files[0].size > 2000000) {
      this.showMessage({
        type: "error",
        text: "Maximum image size is 2 MB"
      });
    } else {
      this.ticket.image = "";
      var reader = new FileReader();
      reader.onloadend = (e: Event) => {
        this.ticket.image = <string>reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  register() {}

  //achar o formato do arquivo
  getType() {
    if (!this.ticket && !this.ticket.image) return "";
    return this.ticket.image.replace("data:", "").split(";")[0];
  }

  //recuperar o arquivo em bytes do banco do back end
  getFile(open?: boolean) {
    const type = this.getType();

    this.ticketService.getFile(this.ticket.id).subscribe((data: any) => {
      const file = new Blob([data], { type: type });

      const fileUrl = URL.createObjectURL(file);

      if (type.includes("pdf") || type.includes("text")) {
        this.urlPdf = fileUrl;
      }

      if (open) {
        //window.open(fileUrl, '_blank', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,height=600,width=800,scrollbars=yes');
        var a = document.createElement("a");
        a.href = fileUrl;
        a.download = this.ticket.id;
        a.dispatchEvent(new MouseEvent("click"));
      }
    });
  }
}
