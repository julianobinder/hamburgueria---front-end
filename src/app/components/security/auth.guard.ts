//essa classe serve para proteger nossas rotas e nossos componentes.
//Exemplo : login só pode ser acessado depois de logado.
//depois de implementada ir para app.routes.ts / e declarar no app.module.ts
import { Observable } from 'rxjs/internal/Observable';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SharedService } from 'src/app/services/shared.service';

@Injectable()
export class AuthGuard implements CanActivate {

    public shared: SharedService;

    constructor(private router: Router) {
        this.shared = SharedService.getInstance();
    }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | boolean {
        //se estiver logado direciona para homepage    
        if (this.shared.isLoggedIn()) {
            return true;
        }
        //senao estiver logado - direciona para tela de login
        this.router.navigate(['/login']);
        return false;
    }
}

