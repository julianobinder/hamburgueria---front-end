import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  //atributos da classe
  body: any = {};
  linkOk = true;

  isLoading = false;
  senhasDiferentes = false;

  constructor(
    private userSvc: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.userSvc.verificarLink(this.route.snapshot.params['id'])
      .subscribe(r => {
        this.linkOk = true;
      }, e => {
        this.linkOk = false;
        alert(e.error.message);
      });
  }

  recuperar() {

    if (this.body.password !== this.body.password2) {
      this.senhasDiferentes = true;
      alert('Senhas diferentes!!');
      return;
    } else {
      this.senhasDiferentes = false;
    }

    this.isLoading = true;
    this.body.linkId = this.route.snapshot.params['id'];
    this.userSvc.recuperarSenha(this.body)
      .subscribe((r: any) => {
        this.isLoading = false;
        alert(r.message);
        this.router.navigate(['/login']);
      });
  }
}

