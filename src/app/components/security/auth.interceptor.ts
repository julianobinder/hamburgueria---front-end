import { Injectable } from '@angular/core';
import { SharedService } from './../../services/shared.service';
import { HttpInterceptor, HttpHandler, HttpEvent, HttpRequest } from "@angular/common/http";
import { Observable } from 'rxjs/internal/Observable';


//com esse intercpetor automatiza todas as requisições que exigem token
//adicionar (essa implementação abaixo no header da requisição HTTP-CLIENT) 
//Exemplo de onde teria que ser feito manual:
//(os servives do front end / user.service.ts / ticket.service.ts )
//depois de criado esse interceptor é preciso registrar em app.module.ts
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    shared: SharedService;

    constructor() {
        this.shared = SharedService.getInstance();
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let authRequest: any;

        //se usuario estiver logado clona a requisição e seta token no header do request 
        //antes de chamar a api(back-end)
        if (this.shared.isLoggedIn()) {
            authRequest = req.clone({
                setHeaders: {
                    'Authorization' : this.shared.token
                }
            })
          return next.handle(authRequest);  
        //senao estiver logado retorna o request mesmo (req: HttpRequest)
        }else {
            return next.handle(req);
        }
    }
}
