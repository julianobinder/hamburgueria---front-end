import { Component, OnInit } from "@angular/core";
import { SendFileService } from "src/app/services/sendfiles.service";
import * as moment from "moment";
import { ConfirmationService } from "primeng/api";
@Component({
  selector: "app-files-received",
  templateUrl: "./files-received.component.html",
  styleUrls: ["./files-received.component.css"]
})
export class FilesReceivedComponent implements OnInit {
  mesAno = moment(new Date()).format("MM-YYYY");

  files: any[] = [];

  file: any = {};

  visualizar = false;
  editStatus = false;

  constructor(
    private svc: SendFileService,
    private confirmationSvc: ConfirmationService
  ) {}

  ngOnInit() {
    this.filtrar();
  }

  filtrar() {
    this.svc
      .findByMesAno(this.mesAno)
      .subscribe((r: any) => (this.files = r.data));
  }

  delete(id) {
    this.confirmationSvc.confirm({
      message: "Confirma deletar este aquivo ?",
      accept: () => {
        this.svc.delete(id).subscribe(r => {
          alert("Arquivo deletado com sucesso!!");
          this.filtrar();
        });
      }
    });
  }

  view(f) {
    this.file = f;
    this.visualizar = true;
  }

  edit(ped) {
    this.file = ped;
    this.editStatus = true;
  }

  save() {
    this.svc.update(this.file).subscribe(r => {
      alert("Status do pedido atualizado com sucesso!");
      this.editStatus = false;
      this.file = {};
    });
  }

  getType() {
    if (!this.file && !this.file.file) return "";
    return this.file.file.replace("data:", "").split(";")[0];
  }

  download() {
    const type = this.getType();

    this.svc.getFile(this.file.id).subscribe((data: any) => {
      const file = new Blob([data], { type: type });

      const fileUrl = URL.createObjectURL(file);

      var a = document.createElement("a");
      a.href = fileUrl;
      a.download = this.file.id;
      a.dispatchEvent(new MouseEvent("click"));
    });
  }
}
