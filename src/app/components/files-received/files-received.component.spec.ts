import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilesReceivedComponent } from './files-received.component';

describe('FilesReceivedComponent', () => {
  let component: FilesReceivedComponent;
  let fixture: ComponentFixture<FilesReceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilesReceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilesReceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
