import { Injectable , EventEmitter} from '@angular/core';
import { User } from '../model/user.model';

// classe vai ser compartilhada- guarda todas informações aqui
//usuario,token
@Injectable()
export class SharedService {

  public static instance: SharedService = null;
  user: User;
  token: string;
  showTemplate = new EventEmitter<boolean>(); //app.component.html

  //retorna nossa instancia
  constructor() {
    return SharedService.instance = SharedService.instance || this;
  }

  //metodo que vai ficar armazenado as informações que vamos colocarmos
  public static getInstance() {
    if (this.instance == null) {
      this.instance = new SharedService();
    }
    return this.instance;
  }

  //verifica se usuario está logado
  isLoggedIn(): boolean {
    if (this.user == null) {
      return false;  //retorna false
    }
    return this.user.email != ''; //retorna true
  }


}
