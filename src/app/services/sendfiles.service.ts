import { SendFilesCustomer } from './../model/send-files';
import { Ticket } from './../model/ticket.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HELP_DESK_API } from './helpdesk.api';

@Injectable()
export class SendFileService {

  constructor(private http: HttpClient) { }




  update(data: SendFilesCustomer) {
    return this.http.post(`${HELP_DESK_API}/api/sendFilesCustomer`, data);
  }


  findByMesAno(mesAno: string) {
    return this.http.get(`${HELP_DESK_API}/api/sendFilesCustomer/${mesAno}`);
  }

  findAll() {
    return this.http.get(`${HELP_DESK_API}/api/sendFilesCustomer/list-all`);
  }

  delete(id: string) {
    return this.http.delete(`${HELP_DESK_API}/api/sendFilesCustomer/${id}`);
  }

  getFile(id: string) {
    return this.http.get(`${HELP_DESK_API}/api/sendFilesCustomer/file/${id}`, { responseType: 'arraybuffer' });
  }

}
