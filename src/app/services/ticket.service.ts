import { Ticket } from './../model/ticket.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HELP_DESK_API } from './helpdesk.api';

@Injectable()
export class TicketService {

  constructor(private http: HttpClient) { }

  //curso
  createOrUpdate(ticket: Ticket) {
    if (ticket.id != null && ticket.id != '') {
      return this.http.put(`${HELP_DESK_API}/api/ticket`, ticket);
    } else {
      ticket.id = null;
      ticket.status = 'New';
      return this.http.post(`${HELP_DESK_API}/api/ticket`, ticket);
    }
  }

  //curso
  findAll(page: number, count: number) {
    return this.http.get(`${HELP_DESK_API}/api/ticket/0/10000000`);
  }

  //curso
  findById(id: string) {
    return this.http.get(`${HELP_DESK_API}/api/ticket/${id}`);
  }

  //curso
  delete(id: string) {
    return this.http.delete(`${HELP_DESK_API}/api/ticket/${id}`);
  }

  //curso
  findByParams(page: number, count: number, t: any) {
    t.number = t.number == null ? 0 : t.number;
    t.title = t.title === '' ? 'undefined' : t.title;
    t.status = t.status === '' ? 'undefined' : t.status;
    t.priority = t.priority === '' ? 'undefined' : t.priority;
    t.mesAno = t.mesAno === '' ? 'undefined' : t.mesAno;
    return this.http.get(`${HELP_DESK_API}/api/ticket/${page}/${count}/${t.number}/${t.title}/${t.status}/${t.priority}/${t.mesAno}`);
  }

  //curso
  changeStatus(status: string, ticket: Ticket) {
    return this.http.put(`${HELP_DESK_API}/api/ticket/${ticket.id}/${status}`, ticket);
  }

  //curso
  summary() {
    return this.http.get(`${HELP_DESK_API}/api/ticket/summary`);
  }

  //chamar service -back end - Juliano
  getFile(id: string) {
     return this.http.get(`${HELP_DESK_API}/api/ticket/file/${id}`, {responseType: 'arraybuffer'});
  }

  //chamar service -back end - Juliano
  //customer pesquisa o arquivo recebido por tipo (agua,luz,telefone) + data a ser pesquisada
  //junto na pesquisa(lá no back-end) é passado o id do cliente que esta fazendo a pesquisa
  findByDateAndTipo(mesAno: any, tipo: string) {
    return this.http.get(`${HELP_DESK_API}/api/ticket/find-by-date-and-tipo/${mesAno}/${tipo}`);
 }


}
