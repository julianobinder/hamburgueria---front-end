import { DialogService } from './dialog-services';
import { AuthGuard } from './components/security/auth.guard';
import { SharedService } from './services/shared.service';
import { UserService } from './services/user.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/security/login/login.component';
import { routes } from './app.routes';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { AuthInterceptor } from './components/security/auth.interceptor';
import { UserNewComponent } from './components/user-new/user-new.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { TicketNewComponent } from './components/ticket-new/ticket-new.component';
import { TicketService } from './services/ticket.service';
import { TicketListComponent } from './components/ticket-list/ticket-list.component';
import { TicketDetailComponent } from './components/ticket-detail/ticket-detail.component';
import { SummaryComponent } from './components/summary/summary.component';
import { ForgotPasswordComponent } from './components/security/forgot-password/forgot-password.component';
import { ArquivosComponent } from './components/arquivos/arquivos.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SendFilesCustomerComponent } from './components/send-files-customer/send-files-customer.component';
import { SendFileService } from './services/sendfiles.service';

// NG Prime
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {CalendarModule} from 'primeng/calendar';
import {InputMaskModule} from 'primeng/inputmask';
import { FilesReceivedComponent } from './components/files-received/files-received.component';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { ListSendFilesComponent } from './components/list-send-files/list-send-files.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    UserNewComponent,
    UserListComponent,
    TicketNewComponent,
    TicketListComponent,
    TicketDetailComponent,
    SummaryComponent,
    ForgotPasswordComponent,
    ArquivosComponent,
    SendFilesCustomerComponent,
    FilesReceivedComponent,
    ListSendFilesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    DropdownModule,
    InputMaskModule,
    MultiSelectModule,
    ConfirmDialogModule,
    CalendarModule,
    TableModule,
    DialogModule,
    ButtonModule,
    routes
  ],
  providers: [
    UserService,
    SharedService,
    DialogService,
    TicketService,
    SendFileService,
    ConfirmationService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
